posibilities = [
    ('scissors', 'cuts', 'paper'),
    ('paper', 'covers', 'rock'),
    ('rock', 'crushes', 'lizard'),
    ('lizard', 'poison', 'spock'),
    ( 'spock', 'smashes', 'scissors'),
    ( 'scissors', 'decapitate', 'lizard'),
    ( 'lizard', 'eats', 'paper'),
    ( 'paper', 'disproves', 'spock'),
    ( 'spock', 'vaporizes', 'rock'),
    ( 'rock', 'crushes', 'scissors'),
]

player1_input, player2_input = "rock", "lizard"


def logicGame(player1_inpu, player2_inpu):
    for posibility in posibilities:
        if player1_input in posibility and player2_input in posibility:
            obj1, does, obj2 = posibility
            return f"{obj1} {does} {obj2}"
        print(posibility) 

logicGame(player1_input, player2_input)