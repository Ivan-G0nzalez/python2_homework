import pytest

from src.employee import *


def test_employee_expects_typeerror():
  match = "Can't instantiate abstract class Employee"

  with pytest.raises(expected_exception=TypeError, match=match):
    Employee(100, "name")


def test_AdmistrativeWorker_calculate():
  worker = {
      'id': 1,
      'name': 'John Doe',
      'role': 'manager',  # is a Admistrative Worker
      'weekly_salary': 1500
  }

  worker = AdmistrativeWorker(
    worker.get("id"),
    worker.get("name"),
    worker.get("weekly_salary"),
  )

  assert worker.calculate_payroll() == 1500, "Salary must be 1500 for Admistrative Worker"
  assert type(worker.calculate_payroll()) is int, "Salary must be 1500 for Admistrative Worker"
  assert type(worker) is AdmistrativeWorker



def test_Manufactoring():
  worker = {
      'id': 2,
      'name': 'Jane Doe',
      'role': 'factory',  # is a Manufacturing Worker
      'worked_hours': 40,
      'hour_rate': 15
  }

  worker = ManufacturingWorker(
      worker.get("id"),
      worker.get("name"),
      worker.get("worked_hours"),
      worker.get("hour_rate")
  )

  assert worker.calculate_payroll() == 600, "Salary must be 1500 for Admistrative Worker"
  assert type(worker.calculate_payroll()) is int, "Salary must be 1500 for Admistrative Worker"
  assert type(worker) is ManufacturingWorker


def test_SalesAssociate():
  worker = {
      'id': 2,
      'name': 'Jane Doe',
      'role': 'factory',  # is a Manufacturing Worker
      'weekly_salary': 1000,
      'commission': 250
  }

  worker = SalesAssociate(
      worker.get("id"),
      worker.get("name"),
      worker.get("weekly_salary"),
      worker.get("commission")
  )
  assert type(worker.calculate_payroll()) is int, "Salary must be 1250 for Admistrative Worker"
  assert type(worker) is SalesAssociate
