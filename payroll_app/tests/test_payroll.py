from src.payroll import PayrollSystem
from src.employee_db import EmployeeDatabase


def test_calculate_payroll_none_list():
  employees = EmployeeDatabase().get_employees()
  payroll_system = PayrollSystem()
  assert payroll_system.calculate_payroll(employees) is not None, "List can not be None"
  
def test_calculate_payroll_emptylist():
  employees = EmployeeDatabase().get_employees()
  payroll_system = PayrollSystem()
  assert len(employees) > 0, "List can't no be empty"


def test_calculate_payroll_non_empty_list():
  employees = EmployeeDatabase().get_employees()
  payroll_system = PayrollSystem()
  assert all(employee is not None for employee in payroll_system.calculate_payroll(employees)), "Object inside the list can not be None"


def test_calculate_payroll_cannot_be_different_than_list():
  employees = EmployeeDatabase().get_employees()
  payroll_system = PayrollSystem()
  assert type(payroll_system.calculate_payroll(employees)) == list, "Calculate payroll cannot be different than a list"
