from abc import ABC, abstractmethod

class Employee(ABC):
    def __init__(self, id, name):
        self.id = id
        self.name = name

    @abstractmethod
    def calculate_payroll(self):
        pass

    def __str__(self) -> str:
        return f"{self.name} {self.id}"


class AdmistrativeWorker(Employee):
    def __init__(self, id, name, weekly_salary=1):
        super().__init__(id, name)
        self.weekly_salary = weekly_salary

    def calculate_payroll(self):
        return self.weekly_salary


class SalesAssociate(AdmistrativeWorker):
    def __init__(self, id, name, weekly_salary, commission):
        super().__init__(id, name, weekly_salary)
        self.commission = commission


    # def __calculate_commission(self):
    #     sales = self.hours_worked / 5
    #     return sales * self.commission


    def calculate_payroll(self):
        fixed = super().calculate_payroll()
        return fixed + self.commission


class ManufacturingWorker(Employee):
    def __init__(self, id, name, hours_worked, hour_rate):
        super().__init__(id, name)
        self.hours_worked = hours_worked
        self.hour_rate = hour_rate

    def calculate_payroll(self):
        return self.hours_worked * self.hour_rate


a = AdmistrativeWorker(1, "ivan")

print(type(a))

# employees = [
#     {
#         'id': 1,
#         'name': 'John Doe',
#         'role': 'manager',  # is a Admistrative Worker
#         'weekly_salary': 1500
#     },
#     {
#         'id': 2,
#         'name': 'Jane Doe',
#         'role': 'factory',  # is a Manufacturing Worker
#         'worked_hours': 40,
#         'hour_rate': 15
#     },
#     {
#         'id': 3,
#         'name': 'Foo Fighter',
#         'role': 'sales',  # is a Sales Associate
#         'fixed_salary': 1000,
#         'comission': 250
#     }
# ]


# workers = []

# for employ in employees:
#     if employ["role"] == 'sales':
#         workers.append(SalesAssociate(employ['id'],employ['name'] ,employ['fixed_salary'],employ['comission']))
#     if employ["role"] == 'factory':
#         workers.append(ManufacturingWorker(employ['id'],employ['name'],employ['worked_hours'],employ['hour_rate']))
#     if employ['role'] == "manager":
#         workers.append(AdmistrativeWorker(employ['id'], employ['name'], employ['weekly_salary']))



# print(workers[2].calculate_payroll())
