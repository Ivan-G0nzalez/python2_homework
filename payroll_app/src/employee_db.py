from src.employee import *

class EmployeeDatabase:
    def __init__(self):
        self.__employees = [
            {
                'id': 1,
                'name': 'John Doe',
                'role': 'manager', # is a Admistrative Worker
                'weekly_salary': 1500
            },
            {
                'id': 2,
                'name': 'Jane Doe',
                'role': 'factory', # is a Manufacturing Worker
                'worked_hours': 40,
                'hour_rate': 15
            },
            {
                'id': 3,
                'name': 'Foo Fighter',
                'role': 'sales', # is a Sales Associate
                'fixed_salary': 1000,
                'comission': 250
            }
        ]


    def get_employees(self):
        """Returns a list of objects whose parent is class Employee"""
        return [self.__create_employee(employee) for employee in self.__employees]

    def __create_employee(self, employee):
        """Returns an object which parent is class Employee"""
      
        if employee['role'] =='manager':
            return AdmistrativeWorker(employee['id'], employee['name'], employee['weekly_salary'])
        if employee['role'] == 'factory':
            return ManufacturingWorker(employee['id'], employee['name'], employee['worked_hours'], employee['hour_rate'])
        if employee['role'] == 'sales':
            return SalesAssociate(employee['id'], employee['name'], employee['fixed_salary'], employee['comission'])

        # for employee in raw_data:
        #     if employee["role"] == 'sales':
        #         workers.append(SalesAssociate(employee['id'],employee['name'] ,employee['fixed_salary'],employee['comission']))
        #     if employee["role"] == 'factory':
        #         workers.append(ManufacturingWorker(employee['id'],employee['name'],employee['worked_hours'],employee['hour_rate']))
        #     if employee['role'] == "manager":
        #         workers.append(AdmistrativeWorker(employee['id'], employee['name'], employee['weekly_salary']))
        # return workers
