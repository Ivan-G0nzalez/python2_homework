class PayrollSystem:

    def calculate_payroll(self, employees):

        result = [ (employee, employee.calculate_payroll()) for employee in employees ]
        [print(f"Payroll for: {e}\n, Check amount: {p}") for e, p in result]
        
        return result
        
        # for employee in employees:
        #     print(f'Payroll for: {employee}')
        #     print(f'Check amount: {employee.calculate_payroll()}')