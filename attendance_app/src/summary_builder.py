from src.format_dict import add_id_value
from src.duration import *
from src.summary import *


def normalize_summary(raw_data: dict):
    format_summary = add_id_value(raw_data)
    return format_summary


def build_summary_object(raw_data: dict):

    format_summary_dict = {k.lower(): v for k, v in raw_data.items()}

    duration_time = Duration(
        format_summary_dict['duration']['hours'],
        format_summary_dict['duration']['minutes'],
        format_summary_dict['duration']['seconds'],
    )

    summary_format = Summary(
        format_summary_dict['title'],
        format_summary_dict['id'],
        format_summary_dict['attended participants'],
        format_summary_dict['start time'],
        format_summary_dict['end time'],
        duration_time
    )

    return summary_format



