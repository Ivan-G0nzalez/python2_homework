class Summary:
    def __init__(self, title, id_participants, participants, start_time, end_time, duration):
        self.title = title
        self.id = id_participants
        self.participants = participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
