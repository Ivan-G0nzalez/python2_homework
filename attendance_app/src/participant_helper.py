import datetime


def total_time_in_meeting(raw):

    format_time = "%m/%d/%y, %H:%M:%S %p"
    time_total = datetime.timedelta()

    for participant in raw:
        start = datetime.datetime.strptime(
            participant['Join Time'], format_time)
        leave = datetime.datetime.strptime(
            participant['Leave Time'], format_time)

        time_total += leave - start

    time = str(time_total).split(":")
    duration_total = f"{time[0]}h {time[1]}m {time[2]}s"

    return duration_total
