from datetime import datetime

def searchKeys(key_search, actual):
    # target_dict = zip(key_mapping, list(new_dictionary.values()))
    for key, value in actual.items():
      if key.lower() == key_search.lower():
        return value
    return actual['Meeting title']


def convert_duration_str_to_dict(new_dictionay):
    start_time_meeting = new_dictionay.get('Start time')
    end_time_meeting = new_dictionay.get('End time')
    format_time = "%m/%d/%y, %H:%M:%S %p"
    h1 = datetime.strptime(start_time_meeting, format_time)
    h2 = datetime.strptime(end_time_meeting, format_time)
    time = str(h2-h1).split(":")
    Duration = {'hours': int(time[0]), 'minutes': int(
        time[1]), 'seconds': int(time[2])}
    return Duration


def add_id_value(new_dictionay):
  time = convert_duration_str_to_dict(new_dictionay)

  final_dictionary = {
      'Title': searchKeys("Meeting Title", new_dictionay),
      "Id": searchKeys("Meeting Id", new_dictionay),
      "Attended participants": int(searchKeys("Attended participants", new_dictionay)),
      "Start Time": new_dictionay.get("Start time"),
      "End Time": new_dictionay.get("End time"),
      "Duration": time
  }
  return final_dictionary


def datetime_parser(string):
    try:
        date_time_format = datetime.strptime(string, "%m/%d/%y, %H:%M:%S %p")
        return date_time_format
    except:
        date_time_format = datetime.strptime(string, "%m/%d/%Y, %H:%M:%S %p")
        return date_time_format



def convert_duration_str_to_dict2(start_time_meeting, end_time_meeting):
    # format_time = "%m/%d/%y, %H:%M:%S %p"

    h1 = datetime_parser(start_time_meeting)
    h2 = datetime_parser(end_time_meeting)
    time = str(h2-h1).split(":")
    Duration = {'Hours': int(time[0]), 'Minutes': int(
        time[1]), 'Seconds': int(time[2])}
    return Duration



