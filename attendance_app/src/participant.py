from src.duration import *


class Participant:
    
    def __init__(self, name, join_time, leave_time, email, role, participant_id , duration=Duration):
        self.full_name = name
        self.join_time = join_time
        self.leave_time = leave_time
        self.email = email
        self.role = role
        self.id = participant_id
        self.in_meeting_duration = duration

        # list_name
        self.complete_name = self.split_name(name)
        
        # name
        self.name = self.set_name(self.complete_name)
        self.middle_name =   self.set_middle_name(self.complete_name)
        self.first_surname =  self.set_first_surname(self.complete_name)
        self.second_surname =  self.set_second_surname(self.complete_name)

    def split_name(self, name):
        if name:
            return name.split(' ')
    
    def set_name(self, splited_name):
        if len(splited_name) > 0:
            return splited_name[0]
    
    def set_middle_name(self, splited_name):
        if len(splited_name) > 3:
            return splited_name[1]

    def set_first_surname(self, splited_name):
        if len(splited_name) == 3:
            return splited_name[-2]
        elif len(splited_name) == 2:
            return splited_name[1]

    def set_second_surname(self, splited_name):
        if len(splited_name) > 2:
            return splited_name[-1]

    

        # if len(self.full_name) == 2:
        #     self.first_surname = self.complete_name[1]
        
        # if len(self.full_name) == 3:
        #     self.second_surname = self.complete_name[2]

        # if len(self.full_name) == 4:
        #     self.middle_name = self.complete_name[1]
        #     self.first_surname = self.complete_name[2]
        #     self.second_surname = self.complete_name[3]


