from src.format_dict import convert_duration_str_to_dict2
from src.participant_helper import total_time_in_meeting
from src.participant import *


def normalize_participant(raw: dict):
    join_time = raw.get("First Join Time") or raw.get("Join Time")
    end_time = raw.get("Leave Time") or raw.get("Last Leave time")
    time = convert_duration_str_to_dict2(join_time, end_time)
    final_dictionary = {
        "Full Name": raw.get("Name") or raw.get("Full Name"),
        "Join time": join_time,
        "Leave time": end_time,
        "In-meeting Duration": time,
        "Email": raw.get("Email"),
        "Role": raw.get("Role"),
        "Participant ID (UPN)": raw.get("Email")
    }

    return final_dictionary

def resolve_participant_join_last_time(raw: list):
    join_time = raw[0].get("First Join Time") or raw[0].get("Join Time")
    end_time = raw[-1].get("Leave Time") or raw[-1].get("Last Leave time")
    time = total_time_in_meeting(raw)
    final_dictionary = {
        "Name": raw[0].get("Name") or raw[0].get("Full Name"),
        "First Join Time": join_time,
        "Last Leave time": end_time,
        "In-meeting Duration": time,
        "Email": raw[0].get("Email"),
        "Role": raw[0].get("Role"),
        "Participant ID (UPN)": raw[0].get("Email")
    }

    return final_dictionary

def build_participant_object(raw: dict):
    
    raw_data = {k.lower(): v for k, v in raw.items()}
    format_duration_time = {k.lower(): v for k, v in raw_data['in-meeting duration'].items()}


    duration_time = Duration(
        format_duration_time['hours'],
        format_duration_time['minutes'],
        format_duration_time['seconds']
    )

    participant = Participant(
        raw_data.get('full name'),
        raw_data.get('join time'),
        raw_data.get('leave time'),
        raw_data.get('email'),
        raw_data.get('role'),
        raw_data.get('participant id (upn)'),
        duration_time
    )

    return participant



# ------------------------------------------------

# def test_build_participant_object_extended():
#     raw = {
#         'Full Name': 'Santiago Martinez Saavedra',
#         'Join time': '10/14/22, 5:51:55 PM',
#         'Leave time': '10/14/22, 7:40:21 PM',
#         'In-meeting Duration': {
#             'hours': 2,
#             'minutes': 3,
#             'seconds': 0
#         },
#         'Email': 'Santiago.Martinez@fundacion-jala.org',
#         'Role': 'Presenter',
#         'Participant ID (UPN)': 'Santiago.Martinez@fundacion-jala.org'
#     }

#     participant = build_participant_object(raw)
#     assert participant is not None
#     assert type(participant) is Participant

#     assert participant.name == 'Santiago'
#     assert participant.middle_name == None
#     assert participant.first_surname == 'Martinez'
#     assert participant.second_surname == 'Saavedra'
#     assert participant.full_name == 'Santiago Martinez Saavedra'

#     assert participant.full_name == raw['Full Name']
#     assert participant.join_time == raw['Join time']
#     assert participant.leave_time == raw['Leave time']
#     assert participant.email == raw['Email']
#     assert participant.role == raw['Role']
#     assert participant.id == raw['Participant ID (UPN)']

#     # in_meeting_duration es un atributo definido en Participant
#     # el tipo de dato de "in_meeting_duration" es Duration (clase)
#     assert type(participant.in_meeting_duration) is Duration
#     assert participant.in_meeting_duration.hours == raw['In-meeting Duration']['hours']
#     assert participant.in_meeting_duration.minutes == raw['In-meeting Duration']['minutes']
#     assert participant.in_meeting_duration.seconds == raw['In-meeting Duration']['seconds']


# test_build_participant_object_extended()
