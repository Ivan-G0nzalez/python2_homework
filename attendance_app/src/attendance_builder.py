from src.attendance import Attendance
from src.summary_builder import build_summary_object
from src.duration import Duration
from src.participant import Participant



def generate_participant(raw_dict):
    raw_data = {k.lower(): v for k, v in raw_dict.items()}

    duration_time = Duration(
        raw_data['duration']['Hours'],
        raw_data['duration']['Minutes'],
        raw_data['duration']['Seconds']
    )

    new_participant = Participant(
        raw_data.get('full name'),
        raw_data.get('join time'),
        raw_data.get('leave time'),
        raw_data.get('email'),
        raw_data.get('role'),
        raw_data.get('participant id (upn)'),
        duration_time,
    )

    return new_participant





def build_attendance_object(raw_attendance):
    raw_data = {k.lower(): v for k, v in raw_attendance.items()} 
    # duration = Duration(
    #     raw_data['duration']['hours'],
    #     raw_data['duration']['minutes'],
    #     raw_data['seconds']['seconds']
    # )

    # summary = Summary(
    #     raw_data['title'],
    #     raw_data['id'],
    #     raw_data['attended participants'],
    #     raw_data['start time'],
    #     raw_data['end time'],
    #     duration
    # )
    summary = build_summary_object(raw_data)

    participants = [generate_participant(participant) for participant in raw_data['participants']]
    
    atendance = Attendance(
        raw_data['start time'],
        summary,
        participants,
    )

    return atendance
